;
; prima partitura: testata
;
sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   islope = 0.1 ; pendenza dell'inviluppo
   iamp = p5
   ifreq = p4
   idur = p3
   iat  = p2
   itable = 1

;          ingresso, salita, dur, discesa
kenv linen  iamp,islope*idur,idur,islope*idur
aout oscil  1, ifreq, itable
     out    aout*kenv
   endin
