from random import random

print("f1 0 8192 10 1")

metro = 126 # 126 alla semiminima
sdur  = 60.0/metro # quanto dura sta semiminima?
ritmo = [0.5, 0.25, 0.25 ] # una croma, due semicrome
dur = 10
cur = 0
cnt = 0

while (cur < dur):
    nota_centrale = random()*100+100
    nota = int(random()*50-25)
    freq = nota_centrale * 2**(nota/12.0)
    amp = random()*0.9+0.1
    step = ritmo[cnt ]*sdur
    cnt += 1
    cnt = cnt % len(ritmo)
    ndur = (step/2.0) + (random()*0.1-0.05)
    print("i1 %8.4f %8.4f %9.4f %8.4f" % (cur, ndur, freq, amp))
    cur = cur + step
