from random import random

print("f1 0 8192 10 1")

metro = 126 # 126 alla semiminima
sdur  = 60.0/metro
ritmo = [0.5, 0.25, 0.25 ]
dur = 10
cur = 0
cnt = 0

while (cur < dur):
    freq = random()*4900+100
    amp = random()*0.9+0.1
    step = ritmo[cnt ]
    cnt += 1
    cnt = cnt % 3
    ndur = (step/2.0) + (random()*0.1-0.05)
    print("i1 %8.4f %8.4f %9.4f %8.4f" % (cur, ndur, freq, amp))
    cur = cur + step
