from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

#
# frequenze
#
fpunto1 = (0.5, 300)
fpunto2 = (15, 3500)
fpunto3 = (0.5, 1500)
fpunto4 = (15,  3550)

bassa = Esponenziale(fpunto1, fpunto2)
alta  = Esponenziale(fpunto3, fpunto4)

#
# metro
#
mpunto1 = (0.5, 5000.0)
mpunto2 = (15,  72.0)

metro = Esponenziale(mpunto1, mpunto2)
start = bassa.x0
fine  = bassa.x1

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    m = metro.y(now)
    step  = 60.0/m
    offset = bassa.y(now)
    range  = alta.y(now) - offset
    freq   = random()*range + offset
    dur    = 0.150
    print("i1 %8.4f %8.4f %8.4f 0.5" % (now, dur, freq))
    buffer_t.append(now)
    buffer_f.append(freq)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_3.png')
