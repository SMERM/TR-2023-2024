from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

punto1 = (0.5, 300)
punto2 = (15, 3500)
punto3 = (0.5, 1500)
punto4 = (15,  3550)

bassa = Esponenziale(punto1, punto2)
alta  = Esponenziale(punto3, punto4)

metro = 540.0
start = bassa.x0
fine  = bassa.x1
step  = 60.0/metro

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    offset = bassa.y(now)
    range  = alta.y(now) - offset
    freq   = random()*range + offset
    dur    = step * 2.2
    print("i1 %8.4f %8.4f %8.4f 0.6" % (now, dur, freq))
    buffer_t.append(now)
    buffer_f.append(freq)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_2.png')
