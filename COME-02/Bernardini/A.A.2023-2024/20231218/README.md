# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 18/12/2023

### Argomenti

* approfondimento su `python`
  * funzioni `python` - programmazione a oggetti
    * funzioni esponenziali e logaritmiche
  * generare partiture con python:
    * realizzazione di maschere di tendenza
  * funzioni modulanti in `csound`

### Lavagne

![exp_mdt.png](./exp_mdt.png)

![exp_log.png](./exp_log.png)

### Codice

[sesta.orc](./sesta.orc)

```csound
sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p5
   ifreq = p4
   idur = p3
   iat  = p2

kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, ifreq
     out    aout
   endin
```

[esponenziale_oop.py](./esponenziale_oop.py)

```python
from math import log, exp

class Esponenziale:

    def __init__(self, p1, p2):
        self.x0 = p1[0]
        self.y0 = p1[1]
        self.x1 = p2[0]
        self.y1 = p2[1]
        self.calcola_coefficienti()

    def calcola_coefficienti(self):
        self.a = (log(self.y1)-log(self.y0))/(self.x1-self.x0)
        self.b = log(self.y0) - (self.x0*self.a)

    def y(self, x):
        return exp(self.a*x + self.b)
```

[mdt_2.py](./mdt_2.py)

```python
from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

punto1 = (0.5, 300)
punto2 = (15, 3500)
punto3 = (0.5, 1500)
punto4 = (15,  3550)

bassa = Esponenziale(punto1, punto2)
alta  = Esponenziale(punto3, punto4)

metro = 540.0
start = bassa.x0
fine  = bassa.x1
step  = 60.0/metro

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    offset = bassa.y(now)
    range  = alta.y(now) - offset
    freq   = random()*range + offset
    dur    = step * 2.2
    print("i1 %8.4f %8.4f %8.4f 0.6" % (now, dur, freq))
    buffer_t.append(now)
    buffer_f.append(freq)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_2.png')
```

Questo script produce la partitura mdt_2.sco e il seguente plot tempo-frequenza

![mdt_2.png](./mdt_2.png)

[mdt_3.py](./mdt_3.py)

```python
from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

#
# frequenze
#
fpunto1 = (0.5, 300)
fpunto2 = (15, 3500)
fpunto3 = (0.5, 1500)
fpunto4 = (15,  3550)

bassa = Esponenziale(fpunto1, fpunto2)
alta  = Esponenziale(fpunto3, fpunto4)

#
# metro
#
mpunto1 = (0.5, 5000.0)
mpunto2 = (15,  72.0)

metro = Esponenziale(mpunto1, mpunto2)
start = bassa.x0
fine  = bassa.x1

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    m = metro.y(now)
    step  = 60.0/m
    offset = bassa.y(now)
    range  = alta.y(now) - offset
    freq   = random()*range + offset
    dur    = 0.150
    print("i1 %8.4f %8.4f %8.4f 0.5" % (now, dur, freq))
    buffer_t.append(now)
    buffer_f.append(freq)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_3.png')
```

![mdt_3.png](./mdt_3.png)

### Studio a casa

* realizzazione delle funzioni logaritmiche prendendo spunto da `esponenziale_oop.py`
* Micro-esercitazione n.5: frammento con sinusoidi singole modulate con attacchi non coincidenti e maschere di tendenza, partitura scritta con `python`
