from math import log, exp

class Esponenziale:

    def __init__(self, p1, p2):
        self.x0 = p1[0]
        self.y0 = p1[1]
        self.x1 = p2[0]
        self.y1 = p2[1]
        self.calcola_coefficienti()

    def calcola_coefficienti(self):
        self.a = (log(self.y1)-log(self.y0))/(self.x1-self.x0)
        self.b = log(self.y0) - (self.x0*self.a)

    def y(self, x):
        return exp(self.a*x + self.b)
