from math import log, exp
from smerm.funzione import Funzione

class Esponenziale (Funzione):

    def calc_y(self):
        return (log(self.y1)-log(self.y0))

    def calcola_coefficienti(self):
        self.a = self.u / self.v   # (vedi condition_parameters()
        self.b = log(self.y0) - (self.x0*self.a)

    def _y(self, x):
        return exp(self.a*x + self.b)
