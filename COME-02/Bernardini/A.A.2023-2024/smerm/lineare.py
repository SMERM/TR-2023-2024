from smerm.funzione import Funzione

class Lineare (Funzione):

    def calc_y(self):
        return (self.x1 - self.x0)

    def calcola_coefficienti(self):
        self.a = self.u / self.v   # (vedi condition_parameters()
        self.b = self.y0 - (self.x0*self.a)

    def _y(self, x):
        return self.a*x + self.b
