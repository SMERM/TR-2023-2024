import sys, os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

import unittest

from smerm.maschera_tf import MascheraTF
from smerm import Esponenziale, Lineare

class MascheraTFTest(unittest.TestCase):

    def setUp(self):
        #              lato a                        lato b                        lato c                       lato d 
        lati = [Lineare((0, 100), (10, 500)), Lineare((10, 500), (12, 40)), Lineare((12, 40), (0, 50)), Lineare((0, 50), (0, 100))]
        self.mtf = MascheraTF(lati)

    def test_create(self):
        self.assertTrue(self.mtf)


if __name__ == '__main__':
    unittest.main()
