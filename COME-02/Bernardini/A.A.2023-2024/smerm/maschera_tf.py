#
# MascheraTF: maschera tempo-frequenza
#
# alla maschera vengono passati i lati della tempo-frequenza in ordine orario.
# Il numero dei lati dovrebbe essere >= 3
#
class MascheraTF:

    def __init__(self, lati):
        self.lati = lati   # lati è un array di funzioni lineari/esponenziali/log ecc.
        self.check_lati()

    def punto(self, t):
        f = 100
        #
        # qui calcola la frequenza
        #
        return f

    def check_lati(self):
        if len(self.lati) < 3:
            raise Exception("lati (%d) must be >= 3" % (self.lati))
