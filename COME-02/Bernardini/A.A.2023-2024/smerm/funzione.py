class Funzione:

    def __init__(self, p1, p2):
        self.x0 = p1[0]
        self.y0 = p1[1]
        self.x1 = p2[0]
        self.y1 = p2[1]
        self.condition_parameters()
        self.calcola_coefficienti()

    def calcola_coefficienti(self):
        raise Exception("la classe Funzione è virtuale pura")

    def y(self, x):
        self.check_range(x)
        return self._y(x)

    def _y(self, x):
        raise Exception("la classe Funzione è virtuale pura")

    def check_range(self, x):
        if (x < self.x0) or (x > self.x1):
            raise Exception("x = %f is out of range (%f, %f)" % (x, self.x0, self.x1))

    #
    # +condition_parameters()+
    # verifica che x0 e x1 non siano uguali, se lo sono mettono la variabile v a 1
    # verifica che y0 e y1 non siano uguali, altrimenti u = y0
    #
    def condition_parameters(self):
        if self.x0 == self.x1:
            self.v = 1
        else:
            self.v = self.x1 - self.x0
        if self.y0 == self.y1:
            self.u = self.y0
        else:
            self.u = self.calc_y()

    def calc_y(self):
        raise Exception("la classe Funzione è virtuale pura")
