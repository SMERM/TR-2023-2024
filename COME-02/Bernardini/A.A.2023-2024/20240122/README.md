# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 22/01/2024

### Argomenti

* Finalizzare lo strumentario di oggetti `python` per i macro-esercizi:
    * creazione della libreria `smerm`
    * principi di ereditarietà degli oggetti
    * tipologie di maschere di tendenza

### Lavagne

![Whiteboard 1](./whiteboard_1.png)

### Codice

Per realizzare la libreria `smerm` abbiamo creato una cartella che abbiamo
posto al di sopra delle cartelle delle lezioni (diventerà comune per tutte le
lezioni), con dentro un file `__init__.py` che è l'elemento necessario a far
diventare la cartella un `package` di `python`.

[smerm.__init__.py](../smerm/__init__.py)

```python
import sys, os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))

import esponenziale
import lineare
from esponenziale import Esponenziale
from lineare import Lineare
```

Abbiamo derivato le nostre funzioni dalla classe `Funzione`:

[smerm.funzione.py](../smerm/funzione.py)

```python
class Funzione:

    def __init__(self, p1, p2):
        self.x0 = p1[0]
        self.y0 = p1[1]
        self.x1 = p2[0]
        self.y1 = p2[1]
        self.condition_parameters()
        self.calcola_coefficienti()

    def calcola_coefficienti(self):
        raise Exception("la classe Funzione è virtuale pura")

    def y(self, x):
        self.check_range(x)
        return self._y(x)

    def _y(self, x):
        raise Exception("la classe Funzione è virtuale pura")

    def check_range(self, x):
        if (x < self.x0) or (x > self.x1):
            raise Exception("x = %f is out of range (%f, %f)" % (x, self.x0, self.x1))

    #
    # +condition_parameters()+
    # verifica che x0 e x1 non siano uguali, se lo sono mettono la variabile v a 1
    # verifica che y0 e y1 non siano uguali, altrimenti u = y0
    #
    def condition_parameters(self):
        if self.x0 == self.x1:
            self.v = 1
        else:
            self.v = self.x1 - self.x0
        if self.y0 == self.y1:
            self.u = self.y0
        else:
            self.u = self.calc_y()

    def calc_y(self):
        raise Exception("la classe Funzione è virtuale pura")
```

[smerm.esponenziale](../smerm/esponenziale.py)

```python
from math import log, exp
from smerm.funzione import Funzione

class Esponenziale (Funzione):

    def calc_y(self):
        return (log(self.y1)-log(self.y0))

    def calcola_coefficienti(self):
        self.a = self.u / self.v   # (vedi condition_parameters()
        self.b = log(self.y0) - (self.x0*self.a)

    def _y(self, x):
        return exp(self.a*x + self.b)
```

[smerm.lineare](../smerm/lineare.py)

```python
from smerm.funzione import Funzione

class Lineare (Funzione):

    def calc_y(self):
        return (self.x1 - self.x0)

    def calcola_coefficienti(self):
        self.a = self.u / self.v   # (vedi condition_parameters()
        self.b = self.y0 - (self.x0*self.a)

    def _y(self, x):
        return self.a*x + self.b
```

[smerm.maschera_tf](../smerm/maschera_tf.py

```python
#
# MascheraTF: maschera tempo-frequenza
#
# alla maschera vengono passati i lati della tempo-frequenza in ordine orario.
# Il numero dei lati dovrebbe essere >= 3
#
class MascheraTF:

    def __init__(self, lati):
        self.lati = lati   # lati è un array di funzioni lineari/esponenziali/log ecc.
        self.check_lati()

    def punto(self, t):
        f = 100
        #
        # qui calcola la frequenza
        #
        return f

    def check_lati(self):
        if len(self.lati) < 3:
            raise Exception("lati (%d) must be >= 3" % (self.lati))
```

Abbiamo realizzato un primo *unit test* (test di regressione) per l'oggetto `MascheraTF` (nella cartella `smerm/test`):

[smerm.test.maschera_tf_test](../smerm/test/maschera_tf_test.py

```python
import sys, os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))

import unittest

from smerm.maschera_tf import MascheraTF
from smerm import Esponenziale, Lineare

class MascheraTFTest(unittest.TestCase):

    def setUp(self):
        #              lato a                        lato b                        lato c                       lato d 
        lati = [Lineare((0, 100), (10, 500)), Lineare((10, 500), (12, 40)), Lineare((12, 40), (0, 50)), Lineare((0, 50), (0, 100))]
        self.mtf = MascheraTF(lati)

    def test_create(self):
        self.assertTrue(self.mtf)


if __name__ == '__main__':
    unittest.main()
```
