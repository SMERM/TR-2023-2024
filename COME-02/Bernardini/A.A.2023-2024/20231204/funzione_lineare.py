alto = [[300, 0.5], [3500, 15]]
a = (alto[1][0]-alto[0][0])/(alto[1][1]-alto[0][1]) # (y1-y0)/(x1-x0)
b = alto[0][0]-(alto[0][1]*a)

t = 0
step = 0.1
end = 20

while(t < end):
    f = t*a+b                     # y = ax+b
    print("%8.1f %8.1f" % (t, f))
    t += step                     # t = t + step (syntactic sugar)
