def lineare(x0,y0,x1,y1,x):
    a = (y1-y0)/(x1-x0)
    b = y0 - (x0*a)
    y = a*x + b
    return y
