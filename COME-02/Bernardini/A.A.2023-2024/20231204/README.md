# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 04/12/2023

### Argomenti

* approfondimento su `python`
  * generare partiture con python:
    * introduzione alle maschere di tendenza
  * funzioni `python` - programmazione strutturata
    * funzioni lineari

### Lavagne

![Maschere di tendenza](./maschere_di_tendenza.png)

### Codice

[Funzione lineare](./funzione_lineare.py)

```python
alto = [[300, 0.5], [3500, 15]]
a = (alto[1][0]-alto[0][0])/(alto[1][1]-alto[0][1]) # (y1-y0)/(x1-x0)
b = alto[0][0]-(alto[0][1]*a)

t = 0
step = 0.1
end = 20

while(t < end):
    f = t*a+b                     # y = ax+b
    print("%8.1f %8.1f" % (t, f))
    t += step                     # t = t + step (syntactic sugar)
```

[lineare.py](./lineare.py)

```python
def lineare(x0,y0,x1,y1,x):
    a = (y1-y0)/(x1-x0)
    b = y0 - (x0*a)
    y = a*x + b
    return y
```

[round.py **ERRATA**](./round.py)

```python
def round(f):
    return int(f+0.5)
```

### Studio a casa

* Micro-esercitazione n.4: frammento con sinusoidi singole con attacchi non coincidenti e maschere di tendenza, partitura scritta con `python`
* correggere la funzione `round` contenuta in `round.py` per renderla corretta (arrotondamenti corretti sia positivi che negativi)
