# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
# Prima annualità (A.A.2023-2024)

## Introduzione (13/11/2023)

## Micro-esercitazioni

### Micro-esercitazione 1 (20/11/2023)

* Ambiente di lavoro (`csound`)
* Esercizio con sinusoidi semplici, scrittura non-algoritmica
* Introduzione a `python`

### Micro-esercitazione 2 (27/11/2023)

* Approfondimento di `python` (funzioni, programmazione strutturata)
* Esercizio con sinusoidi semplici, scrittura algoritmica, maschere di tendenza

### Micro-esercitazione 3 (04/12/2023)

* Approfondimento di `python` (strutture dati, oggetti)
* Esercizio: Uso di suoni concreti e scrittura algoritmica

### Micro-esercitazione 4 (11/12/2023)

* Esercizio: Mix suoni concreti/suoni elettronici, scrittura algoritmica

## Macro-esercitazioni (Milestones)

### Attribuzione del punteggio degli esercizi

| Caratteristica            | Punteggio |
|---------------------------|-----------|
| Musicalità d'insieme      | + 2 punti |
| Realizzazione tecnica     | + 1 punto |
| Qualità sonora            | + 1 punto |
| Puntualità della consegna | + 1 punto |

### Esercizio 1: sinusoidi semplici con maschere di tendenza (+ 5 punti) - 8 gennaio 2024

* solo onde periodiche semplici *non sovrapposte* (non modulate - solo inviluppi trapezoidali)
* vietato utilizzare audio editors
* utilizzare `csound` per la generazione 
* canali d'uscita: 1
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 2-5 minuti
* data di consegna: entro domenica 4 febbraio 2024 ore 13:00
* brano di riferimento: [Fausto Razzi, *Progetto II*](https://open.spotify.com/track/61LcHYVkacI16YhUjXAivk)

### Esercizio 2: sinusoidi semplici modulate con maschere di tendenza (+ 5 punti) - 5 febbraio 2024

* solo onde periodiche semplici *non sovrapposte* con modulazioni a piacere
* vietato utilizzare audio editors
* utilizzare `csound` per la generazione 
* canali d'uscita: 1
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 3-6 minuti
* data di consegna: entro domenica 25 febbraio 2024 ore 13:00
* brano di riferimento: [Gyorgy Ligeti, *Artikulation*](https://youtu.be/71hNl_skTZQ)

### Esercizio 2: collage concreto (algoritmico) (+ 5 punti) - 26 febbraio 2024

* solo frammenti concreti
* vietato utilizzare audio editors
* utilizzare `csound` per la generazione 
* canali d'uscita: 1
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori
* durata: 2-5 minuti
* data di consegna: entro domenica 24 marzo 2024 ore 13:00
* brano di riferimento: [Alain Savouret, *Don Quichotte Corporation*](https://fresques.ina.fr/artsonores/fiche-media/InaGrm00029/alain-savouret-don-quichotte-corporation.html)

### Esercizio 4: somme di sinusoidi modulate con maschere di tendenza (+ 5 punti) - 25 marzo 2024

* somme di sinusoidi con modulazioni a piacere
* vietato utilizzare audio editors
* utilizzare `csound` per la generazione 
* canali d'uscita: ≥ 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 3-10 minuti
* data di consegna: entro domenica 14 aprile 2024 ore 13:00
* brano di riferimento: [Karlheinz Stockhausen, *Studie II*](https://youtu.be/_qi4hgT_d0o)

### Esercizio 5: combinazioni di somme di sinusoidi modulate con maschere di tendenza e suoni concreti (+ 5 punti) - 15 aprile 2024

* somme di sinusoidi con modulazioni a piacere
* suoni concreti
* vietato utilizzare audio editors
* utilizzare `csound` per la generazione 
* canali d'uscita: ≥ 2
* utilizzare `python` (o altro linguaggio di scripting) per la creazione di processi sonori e generazione di maschere
* durata: 5-10 minuti
* data di consegna: entro domenica 12 maggio 2024 ore 13:00
* brano di riferimento: [Barry Truax, *Riverrun*](https://youtu.be/u81IGEFt7dM)

### Esercizio 6: brano d'esame  (+ 5 punti)

* lavoro acusmatico
* vietato utilizzare audio editors
* canali d'uscita: ≥ 2
* durata: 5-10 minuti
* data di consegna: data dell'appello d'esame

# Diario di Bordo

| Studente           | Es.1 | Es.2 | Es.3 | Es.4 | Es.5 | Esame | Voto |
|--------------------|:----:|:----:|:----:|:----:|:----:|:-----:|:----:|
