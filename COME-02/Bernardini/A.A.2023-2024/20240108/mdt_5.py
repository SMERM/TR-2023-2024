from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

#
# frequenze
#
fpunto1 = (0.5, 300)
fpunto2 = (15, 3500)
fpunto3 = (0.5, 1500)
fpunto4 = (15,  3550)

bassa = Esponenziale(fpunto1, fpunto2)
alta  = Esponenziale(fpunto3, fpunto4)

#
# metro
#
mpunto1 = (0.5, 5000.0)
mpunto2 = (15,  72.0)

metro = Esponenziale(mpunto1, mpunto2)
start = bassa.x0
fine  = bassa.x1

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    m = metro.y(now)
    step  = 60.0/m
    dur    = 0.150
    rand = random()
    # calcolo della frequenza iniziale
    offset_start = bassa.y(now)
    range_start  = alta.y(now) - offset_start
    freq_start   = rand*range_start + offset_start
    # calcolo della frequenza finale
    offset_end = bassa.y(now+dur)
    range_end  = alta.y(now+dur) - offset_end
    freq_end   = rand*range_end + offset_end
    print("i1 %8.4f %8.4f 0.5 %10.4f %10.4f" % (now, dur, freq_start, freq_end))
    buffer_t.append(now)
    buffer_f.append(freq_start)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_5.png')
