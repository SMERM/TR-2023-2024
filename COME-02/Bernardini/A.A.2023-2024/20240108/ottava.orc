sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p4
   ifreq_start = p5
   ifreq_end   = p6
   idur = p3
   iat  = p2

kfreq expon  ifreq_start, idur, ifreq_end
kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, kfreq
     out    aout
   endin
