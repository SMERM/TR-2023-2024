sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p5
   ifreq_start = p4
   ifreq_end   = ifreq_start * 1.5
   idur = p3
   iat  = p2

kfreq expon  ifreq_start, idur, ifreq_end
kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, kfreq
     out    aout
   endin
