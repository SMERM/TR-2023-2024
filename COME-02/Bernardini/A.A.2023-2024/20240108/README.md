# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 08/01/2024

### Argomenti

* approfondimento su `python`
  * generare partiture con python:
    * realizzazione di maschere di tendenza
* funzioni modulanti in `csound`

### Lavagne

![maschera_modulata.png](./maschera_modulata.png)

### Codice

[settima.orc](./settima.orc)

```csound
sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p5
   ifreq_start = p4
   ifreq_end   = ifreq_start * 1.5
   idur = p3
   iat  = p2

kfreq expon  ifreq_start, idur, ifreq_end
kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, kfreq
     out    aout
   endin
```

[mdt_4.py](./mdt_4.py)

```python
from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

#
# frequenze
#
fpunto1 = (0.5, 300)
fpunto2 = (15, 3500)
fpunto3 = (0.5, 1500)
fpunto4 = (15,  3550)

bassa = Esponenziale(fpunto1, fpunto2)
alta  = Esponenziale(fpunto3, fpunto4)

#
# metro
#
mpunto1 = (0.5, 5000.0)
mpunto2 = (15,  72.0)

metro = Esponenziale(mpunto1, mpunto2)
start = bassa.x0
fine  = bassa.x1

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    m = metro.y(now)
    step  = 60.0/m
    offset = bassa.y(now)
    range  = alta.y(now) - offset
    freq   = random()*range + offset
    dur    = 0.150
    print("i1 %8.4f %8.4f %8.4f 0.5" % (now, dur, freq))
    buffer_t.append(now)
    buffer_f.append(freq)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_4.png')
```

Questo script produce la partitura mdt_4.sco e il seguente plot tempo-frequenza

![mdt_4.png](./mdt_4.png)

[ottava.orc](./ottava.orc)

```csound
sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p4
   ifreq_start = p5
   ifreq_end   = p6
   idur = p3
   iat  = p2

kfreq expon  ifreq_start, idur, ifreq_end
kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, kfreq
     out    aout
   endin
```

[mdt_5.py](./mdt_5.py)

```python
from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

#
# frequenze
#
fpunto1 = (0.5, 300)
fpunto2 = (15, 3500)
fpunto3 = (0.5, 1500)
fpunto4 = (15,  3550)

bassa = Esponenziale(fpunto1, fpunto2)
alta  = Esponenziale(fpunto3, fpunto4)

#
# metro
#
mpunto1 = (0.5, 5000.0)
mpunto2 = (15,  72.0)

metro = Esponenziale(mpunto1, mpunto2)
start = bassa.x0
fine  = bassa.x1

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    m = metro.y(now)
    step  = 60.0/m
    dur    = 0.150
    rand = random()
    # calcolo della frequenza iniziale
    offset_start = bassa.y(now)
    range_start  = alta.y(now) - offset_start
    freq_start   = rand*range_start + offset_start
    # calcolo della frequenza finale
    offset_end = bassa.y(now+dur)
    range_end  = alta.y(now+dur) - offset_end
    freq_end   = rand*range_end + offset_end
    print("i1 %8.4f %8.4f 0.5 %10.4f %10.4f" % (now, dur, freq_start, freq_end))
    buffer_t.append(now)
    buffer_f.append(freq_start)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_5.png')
```

Questo script produce la partitura mdt_5.sco e il seguente plot tempo-frequenza

![mdt_5.png](./mdt_5.png)

[nona.orc](./nona.orc)

```csound
sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p4
   ifreq_start = p5
   ifreq_end   = p6
   idur = p3
   iat  = p2
   ivib_amp_start = 0.1
   ivib_amp_end   = ifreq_start * 1.5
   ivib_freq = 6 ; 6 Hz

kvibamp expon ivib_amp_start, idur, ivib_amp_end
kvib  oscil  kvibamp, ivib_freq, 1
kfreq expon  ifreq_start, idur, ifreq_end
kfreq = kfreq + kvib
kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, kfreq
     out    aout
   endin
```

[mdt_6.py](./mdt_6.py)

```python
from matplotlib import pyplot as pt
from esponenziale_oop import Esponenziale
from random import random

print("f1 0 8192 10 1")
#
# frequenze
#
fpunto1 = (0.5, 300)
fpunto2 = (15, 3500)
fpunto3 = (0.5, 1500)
fpunto4 = (15,  3550)

bassa = Esponenziale(fpunto1, fpunto2)
alta  = Esponenziale(fpunto3, fpunto4)

#
# metro
#
mpunto1 = (0.5, 5000.0)
mpunto2 = (15,  72.0)

metro = Esponenziale(mpunto1, mpunto2)
start = bassa.x0
fine  = bassa.x1

now   = start
buffer_t = []
buffer_f = []

while (now < fine):
    m = metro.y(now)
    step  = 60.0/m
    dur    = 0.150
    rand = random()
    # calcolo della frequenza iniziale
    offset_start = bassa.y(now)
    range_start  = alta.y(now) - offset_start
    freq_start   = rand*range_start + offset_start
    # calcolo della frequenza finale
    offset_end = bassa.y(now+dur)
    range_end  = alta.y(now+dur) - offset_end
    freq_end   = rand*range_end + offset_end
    print("i1 %8.4f %8.4f 0.5 %10.4f %10.4f" % (now, dur, freq_start, freq_end))
    buffer_t.append(now)
    buffer_f.append(freq_start)
    now    = now + step

pt.plot(buffer_t, buffer_f, '*')
pt.savefig('./mdt_6.png')
```

Questo script produce la partitura mdt_6.sco e il seguente plot tempo-frequenza

![mdt_6.png](./mdt_6.png)

### Studio a casa

* realizzazione delle funzioni logaritmiche prendendo spunto da `esponenziale_oop.py`
* Micro-esercitazione n.6: frammento con sinusoidi singole modulate a piacere con attacchi non coincidenti e maschere di tendenza partitura scritta con `python`
