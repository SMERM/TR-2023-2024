sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p4
   ifreq_start = p5
   ifreq_end   = p6
   idur = p3
   iat  = p2
   ivib_amp_start = 0.1
   ivib_amp_end   = ifreq_start * 1.5
   ivib_freq = 6 ; 6 Hz

kvibamp expon ivib_amp_start, idur, ivib_amp_end
kvib  oscil  kvibamp, ivib_freq, 1
kfreq expon  ifreq_start, idur, ifreq_end
kfreq = kfreq + kvib
kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, kfreq
     out    aout
   endin
