class Lineare:

    def __init__(self, p1, p2):
        self.x0 = p1[0]
        self.y0 = p1[1]
        self.x1 = p2[0]
        self.y1 = p2[1]
        self.calcola_coefficienti()

    def calcola_coefficienti(self):
        self.a = (self.y1-self.y0)/(self.x1-self.x0)
        self.b = self.y0 - (self.x0*self.a)

    def y(self, x):
        return self.a*x + self.b
