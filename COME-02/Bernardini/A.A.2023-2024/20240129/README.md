# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 29/01/2024

### Argomenti

* Problematiche della Composizione Musicale Elettroacustica
  * l'attenzione per il timbro
  * una concezione olistica della composizione
  * comporre con spazi continui
  * astrazione compositiva, suggestioni narrative, sonificazioni, ecc.
  * che cos'è la *musicalità*? Termodinamica dell'informazione musicale
