from lineare_oop import Lineare
from random import random


bassa = Lineare(0.5, 300, 15, 3500)
alta  = Lineare(0.5, 1500, 15, 3550)

metro = 133
start = bassa.x0
fine  = bassa.x1
step  = 60.0/metro

now   = start

while (now < fine):
    offset = bassa.y(now)
    range  = alta.y(now) - offset
    freq   = random()*range + offset
    dur    = step * 2.2
    print("i1 %8.4f %8.4f %8.4f 0.6" % (now, dur, freq))
    now    = now + step
