from lineare import lineare
from random import random

t0_bassa = 0.5
f0_bassa = 300
t1_bassa = 15
f1_bassa = 3500

t0_alta  = t0_bassa
f0_alta  = 1500
t1_alta  = t1_bassa
f1_alta  = 3550

metro = 133
start = t0_bassa
fine  = t1_bassa
step  = 60.0/metro

now   = start

while (now < fine):
    offset = lineare(t0_bassa, f0_bassa, t1_bassa, f1_bassa, now)
    range  = lineare(t0_alta, f0_alta, t1_alta, f1_alta, now) - offset
    freq   = random()*range + offset
    dur    = step * 2.2
    print("i1 %8.4f %8.4f %8.4f 0.6" % (now, dur, freq))
    now    = now + step
