sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p5
   ifreq = p4
   idur = p3
   iat  = p2

kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, ifreq
     out    aout
   endin
