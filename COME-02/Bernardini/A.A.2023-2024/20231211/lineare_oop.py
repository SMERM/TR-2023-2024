class Lineare:

    def __init__(self, x0, y0, x1, y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.calcola_coefficienti()

    def calcola_coefficienti(self):
        self.a = (self.y1-self.y0)/(self.x1-self.x0)
        self.b = self.y0 - (self.x0*self.a)

    def y(self, x):
        return self.a*x + self.b
