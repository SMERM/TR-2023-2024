# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 11/12/2023

### Argomenti

* approfondimento su `python`
  * funzioni `python` - programmazione strutturata
    * funzioni lineari
    * funzioni esponenziali e logaritmiche
  * introduzione alla programmazione a oggetti - strutture dati
    * funzioni per segmenti
  * generare partiture con python:
    * realizzazione di maschere di tendenza
  * funzioni modulanti in `csound`

### Lavagne

![inviluppo esponenziale](./inviluppo_esponenziale.png)

![oop e RAM](./oop_ram.png)

### Codice

[lineare.py](./lineare.py)

```python
def lineare(x0,y0,x1,y1,x):
    a = (y1-y0)/(x1-x0)
    b = y0 - (x0*a)
    y = a*x + b
    return y
```

[mdt_0.py](./mdt_0.py)


```python
from lineare import lineare
from random import random

t0_bassa = 0.5
f0_bassa = 300
t1_bassa = 15
f1_bassa = 3500

t0_alta  = t0_bassa
f0_alta  = 1500
t1_alta  = t1_bassa
f1_alta  = 3550

metro = 133
start = t0_bassa
fine  = t1_bassa
step  = 60.0/metro

now   = start

while (now < fine):
    offset = lineare(t0_bassa, f0_bassa, t1_bassa, f1_bassa, now)
    range  = lineare(t0_alta, f0_alta, t1_alta, f1_alta, now) - offset
    freq   = random()*range + offset
    dur    = step * 2.2
    print("i1 %8.4f %8.4f %8.4f 0.6" % (now, dur, freq))
    now    = now + step
```

[sesta.orc](./sesta.orc)

```csound
sr=48000
ksmps=5
nchnls=1
0dbfs = 1

   instr 1
   iamp = p5
   ifreq = p4
   idur = p3
   iat  = p2

kenv expon   iamp, idur, iamp/1000
aout poscil  kenv, ifreq
     out    aout
   endin
```

Il tutto riscritto in programmazione *orientata agli oggetti*, diventa:

[lineare_oop.py](./lineare_oop.py)

```python
class Lineare:

    def __init__(self, x0, y0, x1, y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.calcola_coefficienti()

    def calcola_coefficienti(self):
        self.a = (self.y1-self.y0)/(self.x1-self.x0)
        self.b = self.y0 - (self.x0*self.a)

    def y(self, x):
        return self.a*x + self.b
```

[mdt_1.py](./mdt_1.py)


```python
from lineare_oop import Lineare
from random import random


bassa = Lineare(0.5, 300, 15, 3500)
alta  = Lineare(0.5, 1500, 15, 3550)

metro = 133
start = bassa.x0
fine  = bassa.x1
step  = 60.0/metro

now   = start

while (now < fine):
    offset = bassa.y(now)
    range  = alta.y(now) - offset
    freq   = random()*range + offset
    dur    = step * 2.2
    print("i1 %8.4f %8.4f %8.4f 0.6" % (now, dur, freq))
    now    = now + step
```

### Studio a casa

* Micro-esercitazione n.4: frammento con sinusoidi singole con attacchi non coincidenti e maschere di tendenza, partitura scritta con `python`
