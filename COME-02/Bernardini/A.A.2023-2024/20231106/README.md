# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 06/11/2023

### Argomenti

* Strutturazione del corso
  * 4 Micro-esercizi
  * 6 macro-esercitazioni
* Ripasso della strumentazione logistica:
  * [calendario SME::Roma](https://calendar.google.com/calendar/u/0?cid=dG41bGs2NmQzcjdsazViaW1ucWxzb2Q1djBAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ)
  * [`git`](https://gitlab.com/SMERM/TR-2023-2024)
  * [`slack`](https://smerm.slack.com) (dettaglio dei canali)
  * [`jitsi`](https://meet.smerm.org/bernardini)
  * [mailing list](https://groups.google.com/g/smerm-studenti)
  * [canale YouTube dedicato](https://www.youtube.com/channel/UCG7pgN812PK0nrAJZQ5FeVA)
* Chiarimenti sulle *funzioni* (vs. i *generi*) della musica
* Problematiche legate all'ascolto odierno: allenamento dell'orecchio
* Problematiche della Composizione Musicale Elettroacustica
  * collocazione nel contesto storico
    * la composizione dopo Cage
    * linguaggio puramente connotativo

### Lavagne

<!--
![TR I 1](./TR_I_20221114_2.jpg)

![TR I 2](./TR_I_20221114.jpg)
-->

### Studio a casa

* allenamento dell'orecchio: una/due ore quotidiane di ascolto di [RadioTre Classica](https://www.raiplayradio.it/radioclassica/) decidendo quando e come distribuirle nella giornata ma **SENZA SCEGLIERE** in base al contenuto.
