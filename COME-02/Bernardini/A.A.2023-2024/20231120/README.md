# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 20/11/2023

### Micro-esercitazione 1

* introduzione a `csound` (fine)
  * modo di funzionamento (*sintesi*, *elaborazione*, *input/output*)
  * struttura dei programmi
    * testata
    * sintassi dell'orchestra
    * sintassi della partitura
  * frequenza di campionamento e frequenza di controllo
  * tipi di variabili dell'orchestra
    * `i`
    * `a`
    * `k`
    * `p`
    * `g`
  * tipi di variabili della partitura
    * `f`
    * `i`
    * tipi minori (`t`, `s`, `e`, ecc.)
  * funzioni elementari dell'orchestra (`ampdbfs`, `cpspch`, ecc.)
  * generatori di funzioni della partitura (`GEN 01`, `GEN 10`, ecc.)
* introduzione a `python`
  * generare partiture con python:
    * partitura `random` controllato
    * introduzione alle maschere di tendenza

### Codice

[quarta.orc](./quarta.orc)<break />
```csound
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
   islope = 0.1 ; pendenza dell'inviluppo
   iamp = p5
   ifreq = p4
   idur = p3
   iat  = p2

;          ingresso, salita, dur, discesa
kenv linen  iamp,islope*idur,idur,islope*idur
aout poscil 1, ifreq
     out    aout*kenv
   endin
```

[quarta.sco](./quarta.sco)<break />
```csound
;p1 p2 p3 p4  p5
i1 1.5 3 300 0.38
i1 0.01 1 321 0.01
```

[quinta.orc](./quinta.orc)<break />
```csound
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
   islope = 0.1 ; pendenza dell'inviluppo
   iamp = p5
   ifreq = p4
   idur = p3
   iat  = p2
   itable = 1

;          ingresso, salita, dur, discesa
kenv linen  iamp,islope*idur,idur,islope*idur
aout oscil  1, ifreq, itable
     out    aout*kenv
   endin
```

[quinta.sco](./quinta.sco)<break />
```csound
f1 0 8192 10 1
;p1 p2 p3 p4  p5
i1 1.5 3 300 0.38
i1 0.01 1 321 0.01
```

[primo.py](./primo.py)<break />
```python
print("f1 0 8192 10 1")
print("i1 0 0.3 440 0.38")
```

Eseguendo le seguenti righe di codice:

```bash
$ python primo.py > quinto-primo.sco
$ csound -Wo ./quinto-primo.wav quinta.orc quinto-primo.sco
```

otteniamo prima la partitura `csound` e poi il file .wav.

[quinto-primo.sco](./quinto-primo.sco)<break />
```csound
f1 0 8192 10 1
i1 0 0.3 440 0.38
```

[secondo.py](./secondo.py)<break />
```python
import random

print("f1 0 8192 10 1")

dur = 10
cur = 0
step = 0.2

while (cur < dur):
    freq = random.random()*4900+100
    amp = random.random()
    ndur = step / 2
    print("i1 %8.4f %8.4f %8.4f %8.4f" % (cur, ndur, freq, amp))
    cur = cur + step
```

Eseguendo le seguenti righe di codice:

```bash
$ python secondo.py > quinto-secondo.sco
$ csound -Wo ./quinto-secondo.wav quinta.orc quinto-secondo.sco
```

otteniamo prima la partitura `csound` e poi il file .wav.

[quinto-secondo.sco](./quinto-secondo.sco)<break />
```csound
f1 0 8192 10 1
i1   0.0000   0.1000 4789.4875   0.5658
i1   0.2000   0.1000 3291.7674   0.1452
i1   0.4000   0.1000 3372.0487   0.6938
i1   0.6000   0.1000 3521.4332   0.0981
i1   0.8000   0.1000 4786.6465   0.6957
i1   1.0000   0.1000 3024.5560   0.8565
i1   1.2000   0.1000 1555.1079   0.1014
i1   1.4000   0.1000 4598.7955   0.6920
i1   1.6000   0.1000 385.3554   0.2514
i1   1.8000   0.1000 4273.3081   0.0918
i1   2.0000   0.1000 414.7598   0.8884
i1   2.2000   0.1000 2278.1390   0.1476
i1   2.4000   0.1000 1837.1170   0.8569
i1   2.6000   0.1000 3271.7631   0.2786
i1   2.8000   0.1000 2205.5751   0.8610
i1   3.0000   0.1000 4511.5661   0.1939
i1   3.2000   0.1000 678.9239   0.1798
i1   3.4000   0.1000 3615.5720   0.8510
i1   3.6000   0.1000 3091.7227   0.1857
i1   3.8000   0.1000 3421.7871   0.2079
i1   4.0000   0.1000 3528.9500   0.2763
i1   4.2000   0.1000 4017.8670   0.7223
i1   4.4000   0.1000 3465.7868   0.1730
i1   4.6000   0.1000 989.2602   0.6785
i1   4.8000   0.1000 2410.4649   0.7538
i1   5.0000   0.1000 4661.1113   0.9117
i1   5.2000   0.1000 3057.1266   0.5338
i1   5.4000   0.1000 2554.7288   0.9316
i1   5.6000   0.1000 1155.4978   0.9510
i1   5.8000   0.1000 4121.1491   0.2223
i1   6.0000   0.1000 821.0583   0.3666
i1   6.2000   0.1000 4844.5356   0.2378
i1   6.4000   0.1000 916.3805   0.3956
i1   6.6000   0.1000 785.5679   0.3628
i1   6.8000   0.1000 1053.3305   0.0024
i1   7.0000   0.1000 3872.3688   0.5260
i1   7.2000   0.1000 3351.3816   0.6571
i1   7.4000   0.1000 3733.7049   0.9128
i1   7.6000   0.1000 4810.1138   0.2705
i1   7.8000   0.1000 4420.0134   0.4229
i1   8.0000   0.1000 3349.2852   0.6516
i1   8.2000   0.1000 1056.8157   0.2093
i1   8.4000   0.1000 1224.1139   0.4262
i1   8.6000   0.1000 358.4980   0.8335
i1   8.8000   0.1000 2995.0438   0.1819
i1   9.0000   0.1000 1437.1270   0.6411
i1   9.2000   0.1000 2291.3054   0.1614
i1   9.4000   0.1000 1785.1641   0.3591
i1   9.6000   0.1000 2392.9118   0.2369
i1   9.8000   0.1000 850.5716   0.9822
i1  10.0000   0.1000 2723.8005   0.0765
```

Lo spettrogramma del frammento risultante è:

![quinto-secondo.png](./quinto-secondo.png)<break />

### Studio a casa

* Micro-esercitazione n.2: frammento con sinusoidi singole con attacchi non coincidenti, partitura scritta con `python`
