;
; prima partitura: testata
;
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
   islope = 0.1 ; pendenza dell'inviluppo
   iamp = p5
   ifreq = p4
   idur = p3
   iat  = p2

;          ingresso, salita, dur, discesa
kenv linen  iamp,islope*idur,idur,islope*idur
aout poscil 1, ifreq
     out    aout*kenv
   endin
