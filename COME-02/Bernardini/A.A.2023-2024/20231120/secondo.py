import random

print("f1 0 8192 10 1")

dur = 10
cur = 0
step = 0.2

while (cur < dur):
    freq = random.random()*4900+100
    amp = random.random()
    ndur = step / 2
    print("i1 %8.4f %8.4f %9.4f %8.4f" % (cur, ndur, freq, amp))
    cur = cur + step
