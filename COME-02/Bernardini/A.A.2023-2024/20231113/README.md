# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 13/11/2023

### Micro-esercitazione 1

* introduzione a `csound`
  * modo di funzionamento (*sintesi*, *elaborazione*, *input/output*)
  * struttura dei programmi
    * testata
    * sintassi dell'orchestra
    * sintassi della partitura
  * frequenza di campionamento e frequenza di controllo
* primo script `csound`: un oscillatore sinusoidale e note singole
* secondo script `csound`: un oscillatore sinusoidale e note sovrapposte

### Lavagne

![csound_schema.png](./csound_schema.png)

![csound_orchestra.png](./csound_orchestra.png)

![inviluppo.png](./inviluppo.png)

### Codice `csound`

[prima.orc](./prima.orc)
```csound
;
; prima partitura: testata
;
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
aout poscil 0.5, 1000
     out    aout
   endin
```
[prima.sco](./prima.sco)
```csound
i1 0.03 3
i1 0.01 1
```
[seconda.orc](./seconda.orc)
```csound
;
; prima partitura: testata
;
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
aout poscil 0.5, 1000
;          ingresso, salita, dur, discesa
aout linen  aout,0.1*p3,p3,0.1*p3
     out    aout
   endin

```

[terza.orc](./terza.orc)
```csound
;
; prima partitura: testata
;
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
aout poscil 0.5, p4
;          ingresso, salita, dur, discesa
aout linen  aout,0.1*p3,p3,0.1*p3
     out    aout
   endin
```

[terza.sco](./terza.sco)
```csound
i1 0.03 3 300
i1 0.01 1 321
```

[quarta.orc](./quarta.orc)
```csound
;
; prima partitura: testata
;
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
aout poscil p5, p4
;          ingresso, salita, dur, discesa
aout linen  aout,0.1*p3,p3,0.1*p3
     out    aout
   endin
```

[quarta.sco](./quarta.sco)
```csound
i1 1.5 3 300 0.38
i1 0.01 1 321 0.01
```

### Studio a casa

* Micro-esercitazione n.1: frammento con sinusoidi singole con attacchi non coincidenti, scritto "a mano" (frequenze a piacere, durate a piacere)
