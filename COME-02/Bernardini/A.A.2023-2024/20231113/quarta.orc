;
; prima partitura: testata
;
sr=48000
ksmps=100
nchnls=1
0dbfs = 1

   instr 1
aout poscil p5, p4
;          ingresso, salita, dur, discesa
aout linen  aout,0.1*p3,p3,0.1*p3
     out    aout
   endin
